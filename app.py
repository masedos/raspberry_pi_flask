from flask import Flask, render_template , request, url_for
import RPi.GPIO as GPIO

led_pin = 23 
GPIO.setmode(GPIO.BCM)   
GPIO.setup(led_pin , GPIO.OUT)

app = Flask(__name__)

@app.route("/")                 

@app.route("/", methods=['POST'])  

def mymenu():  
	if request.method == 'POST':  
		submitted_value = request.form['myform']  
		
		if submitted_value =="Led_on": 
			GPIO.output(led_pin, True)		
			
		if submitted_value =="Led_off": 
			GPIO.output(led_pin, False)
		
		if submitted_value =="Reboot": 
			print ("Add your code in for reboot ect here... ")
		
		print ("Button value :   ", submitted_value)  

	return render_template('base.html') 

@app.route("/about/")
def about():
    return render_template('about.html')
     
if __name__ == "__main__":
   app.run(host='0.0.0.0', port=80, debug=False)
